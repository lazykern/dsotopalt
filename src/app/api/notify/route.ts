import { NextRequest, NextResponse } from "next/server";

export const POST = async (req: NextRequest, res: NextResponse) => {
  const data = await req.json();
  const name = data.name;
  const food = data.food;
  const interest = data.interest;

  console.log(`Name: ${name}\nFood: ${food}\nInterest: ${interest}`);

  return await fetch(
    'https://notify-api.line.me/api/notify?message=' + encodeURIComponent(`Name: ${name}\nFood: ${food}\nInterest: ${interest}`),
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': `Bearer ${process.env.LINE_TOKEN}`
      },
    }
  )
}
