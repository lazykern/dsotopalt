'use client';

import { getCookie, setCookie } from 'cookies-next';
import notifySDK from 'line-notify-sdk';
import { cookies } from 'next/headers';
import { redirect, useRouter } from 'next/navigation';
import { useEffect, useState } from 'react';
import { MdArrowDownward, MdArrowUpward } from 'react-icons/md';

export default function Home() {

  const [name, setName] = useState(getCookie('name') || '');
  const [food, setFood] = useState(getCookie('food') || '');
  const [interest, setInterest] = useState(getCookie('interest') || '');
  const [showHint, setShowHint] = useState(getCookie('showHint') === '1' || false);
  const [numHelpClick, setNumHelpClick] = useState(Number(getCookie('numHelpClick') ?? 0));
  const [showHelper, setShowQR] = useState(getCookie('showQR') === '1' || false);
  const [isDataChanged, setIsDataChanged] = useState(false);

  const helpers = [
    "ลองหาจาก 100 105 115 99 111 114 100 10",
    "ไม่ใช่ปี 2",
    "801",
    "ครึ่งนึงแปลครึ่งนึงทับศัพธ์",
  ]

  useEffect(() => {
    setIsDataChanged(true);
  }, [name, food, interest]);

  const router = useRouter();


  const scrollToName = () => {
    document.getElementById('name')?.scrollIntoView({ behavior: 'smooth' });
    document.getElementById('name')?.querySelector('input')?.focus();
  }

  const scrollToFood = () => {
    document.getElementById('food')?.scrollIntoView({ behavior: 'smooth' });
    document.getElementById('food')?.querySelector('input')?.focus();
  }

  const scrollToInterest = () => {
    document.getElementById('interest')?.scrollIntoView({ behavior: 'smooth' });
    document.getElementById('interest')?.querySelector('textarea')?.focus();
  }

  const scrollToConfirm = () => {
    router.push('#confirm');
  }

  const scrollToHint = () => {
    router.push('#hint');
  }

  // scroll to latest section
  useEffect(() => {
    if (!name && !food && !showHint) {
      return;
    }
    const sections = document.querySelectorAll('section');
    const lastSection = sections[sections.length - 1];
    lastSection.scrollIntoView({ behavior: 'smooth' });
  }, []);

  return (
    <main className="snap-y snap-mandatory overflow-y-scroll h-screen no-scrollbar max-w-screen">
      <section className="h-svh flex items-center justify-center snap-start" id="start">
        <div className="text-center flex flex-col mx-4">
          <h1 className="text-5xl font-bold">สวัสดีครับ</h1>
          <p className="text-2xl mt-4">ก่อนจะดูคำใบ้ ขอถามอะไรเล็กๆน้อยๆก่อนนะครับ :)</p>
          <p className="text-sm mt-4">เลื่อนลงได้เลยย</p>
        </div>
      </section>
      <section className="h-svh flex items-center justify-center snap-start" id="name">
        <div className="text-center flex flex-col mx-4">
          <h2 className="text-2xl">น้องชื่ออะไรครับ?</h2>
          <p className="text-sm mt-4 text-red-400 ">โกหกไม่เลี้ยงข้าวนะครับ</p>
          <form
            className="flex flex-col items-center"
            onSubmit={(e) => {
              e.preventDefault();
              scrollToFood();
            }
            }>
            <input
              type="text"
              className="border border-gray-600 rounded p-2 mt-4"
              value={name}
              onChange={(e) => {
                setName(e.target.value)
                setCookie('name', e.target.value)
              }}
              required
            />
            <button type="submit" className="button mt-1 text-white px-4 py-2 rounded disabled:opacity-50 bg-blue-500 hover:bg-blue-600" disabled={!name}
              onClick={() => {
                scrollToFood();
              }}
            >
              ไปต่อ
            </button>
          </form>
        </div>
      </section>
      {
        name &&
        <section className="h-svh flex items-center justify-center snap-start" id="food">
          <div className="text-center flex flex-col mx-4">
            <h2 className="text-2xl">น้อง{name}ชอบกินอะไรครับ?</h2>
            <p className="text-sm mt-4">ตัวอย่าง {'"sushi ramen steak ข้าวหมูทอดกระเทียม ทาโกยากิ"'}</p>
            <form
              className="flex flex-col items-center"
              onSubmit={(e) => {
                e.preventDefault();
                scrollToInterest();
              }
              }>
              <input
                type="text"
                className="border border-gray-600 rounded p-2 mt-4"
                value={food}
                onChange={(e) => {
                  setFood(e.target.value)
                  setCookie('food', e.target.value)
                }}
                required
              />
              <button type="submit" className="button mt-1 text-white px-4 py-2 rounded disabled:opacity-50 bg-blue-500 hover:bg-blue-600" disabled={!food}
                onClick={() => {
                  scrollToInterest();
                }}
              >
                ไปต่อ
              </button>
            </form>
          </div>
        </section>
      }
      {
        name && food &&
        <section className="h-svh flex items-center justify-center snap-start" id="interest">
          <div className="text-center flex flex-col mx-4">
            <h2 className="text-2xl">น้อง{name}ชอบอะไรครับ?</h2>
            <p className="text-sm mt-4">ไม่ว่าจะงานอะดิเรก เทคโนโลยี ศิลปินที่ชอบ เกมที่เล่น การ์ตูน หรือ Fandom ไหน บอกได้เลยเต็มที่ครับ (ตามความสะดวก)
            </p>
            <form
              className="flex flex-col"
              onSubmit={(e) => {
                e.preventDefault();
                scrollToConfirm();
              }
              }>
              <textarea
                className="border border-gray-600 rounded p-2 mt-4 h-48 max-h-96"
                value={interest}
                onChange={(e) => {
                  setInterest(e.target.value)
                  setCookie('interest', e.target.value)
                }}
              />
              <button type="submit" className="button mt-1 text-white px-4 py-2 rounded disabled:opacity-50 bg-blue-500 hover:bg-blue-600 max-w-xs mx-auto"
                onClick={() => {
                  scrollToConfirm();
                }}
              >
                ไปต่อ
              </button>
            </form>
          </div>
        </section>
      }
      {
        name && food &&
        <section className="h-svh flex flex-col items-center justify-center snap-start" id="confirm">
          <h2 className="text-2xl">ตรวจสอบความถูกต้อง</h2>
          <div className="mt-4 grid grid-cols-2 gap-5 max-w-2xl whitespace-pre mx-4 overflow-x-auto">
            <a
              className="underline text-blue-500"
              onClick={() => {
                scrollToName();
              }}>
              <h3 className="text-xl text-right">ชื่อ</h3></a>
            <p className='mt-1.5'>{name}</p>
            <a
              className="mt-1.5 underline text-blue-500"
              onClick={() => {
                scrollToFood();
              }}>
              <h3 className="text-xl text-right">อาหารที่ชอบ</h3></a>
            <p className='mt-1.5'>{food}</p>
            <a
              className="mt-1.5 underline text-blue-500"
              onClick={() => {
                scrollToInterest();
              }}>
              <h3 className="text-xl text-right">สิ่งที่ชอบ</h3></a>
            <p className='mt-1.5'>{interest || 'ไม่มี'}</p>
          </div>
          <button className="button mt-8 text-white px-4 py-2 rounded disabled:opacity-50 bg-blue-500 hover:bg-blue-600" disabled={!name || !food || !isDataChanged}
            onClick={() => {
              setShowHint(true)
              setCookie('showHint', showHint ? '0' : '1')
              setIsDataChanged(false);
              submit(name, food, interest);
              (new Promise(res => setTimeout(res, 1000))).then(() => {
                scrollToHint();
              });
            }}
          >
            {
              showHint ? 'ส่งซ้ำ' :
                'ส่งคำตอบและดูคำใบ้'
            }
          </button>
          {
            showHint &&
            <p className='text-sm mt-4' onClick={scrollToHint}>เลื่อนลงง</p>
          }
        </section>
      }
      {
        name && food && showHint &&
        <section className="h-svh flex flex-col items-center justify-center snap-start" id="hint">
          <div className="text-center flex flex-col">
            <h3 className="text-xl mb-6">คำใบ้คือ..</h3>
            <h1 className="text-6xl">ขี้เกียจเกิน</h1>
          </div>
          {
            <>
              <button
                className={`button mt-8 text-white px-4 py-2 rounded bg-blue-500 hover:bg-blue-600 disabled:opacity-50`}
                style={
                  {
                    filter: `blur(${Math.max(0, 30 - numHelpClick)}px)`,
                  }
                }
                onClick={() => {
                  setNumHelpClick(numHelpClick + 1)
                  setCookie('numHelpClick', (numHelpClick + 1).toString())
                  if (numHelpClick >= 30) {
                    setCookie('showQR', '1')
                  }
                }}
              >
                Need help? คลิกที่นี่!
              </button>
              <p
                className="text-[2px]"
              >click สีฟ้า ซ้ำ</p>
            </>
          }
          {
            showHelper &&
            <div className="mt-4 max-w-xs">
              {
                helpers[Math.floor(Math.random() * helpers.length)]
              }
            </div>
          }
        </section>
      }
      <div className="absolute bottom-0 left-0 p-4 text-sm">
        {numHelpClick}
      </div>
    </main >
  );
}

function submit(name: string, food: string, interest?: string) {
  fetch('/api/notify', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      name,
      food,
      interest,
    }),
  });
}

